package ui 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import entities.PlayerEntity;
	
	public class UICashEntity extends Entity 
	{
		private var uiText:Text;
		
		public function UICashEntity() 
		{
			x = FP.world.camera.x + 100;
			y = FP.world.camera.y + 20;
			
			uiText = new Text("0");
			uiText.color = 0xffffff;
			uiText.size = 24;
			graphic = uiText;
			
			var coin:Image = new Image(A.IMAGE_COIN);
			coin.x = -50;
			coin.y = -4;
			addGraphic(coin);
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			x = FP.world.camera.x + 100;
			y = FP.world.camera.y + 20;
			
			var player:Array = new Array;
			
			FP.world.getClass(PlayerEntity, player);
			
			for each(var pl:PlayerEntity in player)
			{
				uiText.text = String(pl.wallet.amount);
			}

			super.update();
		}
	}

}