package ui 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	import entities.BankEntity;
	
	/**
	 * ...
	 * @author ...
	 */
	public class UIBankEntity extends Entity 
	{
		private var uiText:Text;
		
		public function UIBankEntity() 
		{
			x = FP.world.camera.x + 540;
			y = FP.world.camera.y + 20;
			
			uiText = new Text("0");
			uiText.color = 0xffffff;
			uiText.size = 24;
			graphic = uiText;
			
			var icon:Image = new Image(A.IMAGE_BICON);
			icon.x = -50;
			icon.y = -4;
			addGraphic(icon);
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			x = FP.world.camera.x + 500;
			y = FP.world.camera.y + 20;
			
			uiText.text = String(BankEntity.totalMoney);
			
			super.update();
		}
	}

}