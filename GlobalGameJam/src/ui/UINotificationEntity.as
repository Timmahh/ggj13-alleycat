package ui 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class UINotificationEntity extends Entity 
	{
		private var uiText:Text;
		
		public var shop:Boolean = false;
		public var bank:Boolean = false;
		
		public function UINotificationEntity() 
		{
			layer = 0;
			x = FP.world.camera.x + 320;
			y = FP.world.camera.y + 200;
			
			uiText = new Text("");
			uiText.color = 0xffffff;
			uiText.size = 18;
			uiText.align = "center";
			graphic = uiText;
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			x = FP.world.camera.x + 160;
			y = FP.world.camera.y + 80;
			
			if (shop)
			{
				uiText.text = "Would you like to buy a hotdog?\n10 GOLD\n<<SPACE>>";
			} 
			else if (bank)
			{
				uiText.text = "Would you like to deposit your money?\n<<SPACE>>";
			}
			else
			{
				uiText.text = "";
			}
			
			super.update();
		}
		
		public function shopMessage():void
		{
			uiText.text = "Would you like to buy a hotdog? \n<<SPACE>>";
		}
	}

}