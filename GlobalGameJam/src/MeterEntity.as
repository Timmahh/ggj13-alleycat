package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class MeterEntity extends Entity 
	{
		private var im:Spritemap = new Spritemap(A.IMAGE_METER, 30, 350);
		public function MeterEntity() 
		{
			layer = -1;
			im.add("play", [0, 1], 10, true);
			im.play("play");
			graphic = im;
			x = FP.world.camera.x + 595;
			y = FP.world.camera.y + 50;
			
			super(x, y, graphic, mask);
			
		}
		
		override public function update():void 
		{
			x = FP.world.camera.x + 595;
			y = FP.world.camera.y + 50;
			
			super.update();
		}
	}

}