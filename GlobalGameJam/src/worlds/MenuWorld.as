package worlds 
{
	import entities.playerComponents.HeartbeatComponent;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.sound.SfxFader;
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class MenuWorld extends World 
	{
		private var background:Backdrop = new Backdrop(A.IMAGE_BACK);
		private var cat:Spritemap = new Spritemap(A.IMAGE_TITLE, 640, 480);
		
		private var currentSelect:int = 1;
		
		private var play:Text = new Text("Play", 430, 280);
		private var tutorial:Text = new Text("Tutorial", 430, 330);
		
		private var pointer:Entity = new Entity(410, 289, new Image(A.IMAGE_POINTER));
		
		private var menuMusic:Sfx = new Sfx(A.music);
		
		public function MenuWorld() 
		{
			super();
			
			cat.add("play", [0, 1, 2, 3], 10);
			cat.play("play");
			
			play.size = 36;
			tutorial.size = 36;
		}
		
		override public function begin():void 
		{
			addGraphic(background);
			addGraphic(cat);
			
			addGraphic(play);
			addGraphic(tutorial);
			
			add(pointer);
			
			menuMusic.play();
			
			super.begin();
		}
		
		override public function end():void 
		{
			menuMusic.stop();
			
			super.end();
		}
		
		override public function update():void 
		{
			handleInput();
			
			switch(currentSelect)
			{
				case 1:
					pointer.y = 289;
					break;
				case 2:
					pointer.y = 339;
					break;
				default:
					pointer.y = 289;
					break;
			}
			
			super.update();
		}
		
		public function handleInput():void
		{
			if (Input.pressed(Key.DOWN))
			{
				currentSelect++;
			}
			
			if (Input.pressed(Key.UP))
			{
				currentSelect--;
			}
			
			if (currentSelect >= 3)
			{
				currentSelect = 1;
			}
			else if (currentSelect <= 0)
			{
				currentSelect = 2;
			}
			
			if (Input.pressed(Key.ENTER) || Input.pressed(Key.SPACE))
			{
				if (currentSelect == 1)
				{
					FP.world = new GameWorld;
				}
				if (currentSelect == 2)
				{
					FP.world = new TutorialWorld;
				}
			}
		}
	}

}