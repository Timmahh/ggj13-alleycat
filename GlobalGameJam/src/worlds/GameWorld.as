package worlds 
{
	import entities.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import ui.UIBankEntity;
	import ui.UICashEntity;
	
	public class GameWorld extends World
	{
		private var level:LevelEntity;
		private var player:PlayerEntity;
		
		private var backMusic:Sfx = new Sfx(A.music, repeat);
		
		public function GameWorld() 
		{			
			super();			
		}
		
		override public function begin():void 
		{
			super.begin();
			
			var mapXML:XML = FP.getXML(A.LEVEL);
			
			level = new LevelEntity(mapXML);
			add(level);
			add(new LightmapEntity(mapXML));
			
			player = new PlayerEntity(int(mapXML.Entities.Player.@x), int(mapXML.Entities.Player.@y));
			add(player);
			
			add(new LifeBarEntity(600, 450, player));
			add(new MeterEntity());
			
			for each(var node:XML in mapXML.Entities.Lamp)
			{
				addGraphic(new Image(A.IMAGE_LAMP), 1, int(node.@x), int(node.@y));
			}
			
			for each(var node:XML in mapXML.Entities.Shop)
			{
				add(new ShopEntity(int(node.@x), int(node.@y)));
			}
			
			for each(var node:XML in mapXML.Entities.Bank)
			{
				add(new BankEntity(int(node.@x), int(node.@y)));
			}
			
			createSpawns();
			add(new UICashEntity());
			add(new UIBankEntity());
			
			backMusic.play();
		}
		
		override public function end():void 
		{
			backMusic.stop();
			
			super.end();
		}

		override public function update():void 
		{
			camera.x = FP.clamp(player.x - 320, 0, 1280 - 640);
			camera.y = FP.clamp(player.y - 240, 0, 960 - 480);
			
			
			super.update();
		}
		
		public function createSpawns():void
		{
			add(new PedestrianSpawnEntity( -64, 160, 1));
			add(new CarSpawnEntity( -64, 224, 1));
			add(new CarSpawnEntity( 1280 + 64, 288, -1));
			add(new PedestrianSpawnEntity( 1280 + 64, 352, -1));
			
			add(new PedestrianSpawnEntity( -64, 608, 1));
			add(new CarSpawnEntity( -64, 672, 1));
			add(new CarSpawnEntity( 1280 + 64, 736, -1));
			add(new PedestrianSpawnEntity( 1280+64, 800, -1));
		}
		
		
		
		public function repeat():void
		{
			backMusic.play();
		}
	}

}