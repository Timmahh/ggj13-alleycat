package worlds 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Sfx;
	import net.flashpunk.World;
	import entities.BankEntity;
	
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class DeathWorld extends World
	{
		private var heartbeat:Sfx = new Sfx(A.SFX_DEATH, explosion);
		private var explode:Sfx = new Sfx(A.SFX_EXPLOSION, displayStats); 
		private var beat:Spritemap;
		private var heart:Spritemap = new Spritemap(A.IMAGE_EXPLOSION, 640, 480);
		
		private var ret:Boolean = false;
		
		public function DeathWorld() 
		{
			super();
		}
		
		override public function begin():void 
		{
			heartbeat.play();
			beat = new Spritemap(A.IMAGE_HEART, 300, 300);
			beat.add("beating", [0, 1, 2, 3], 10);
			beat.play("beating");
			heart.add("expl", [0, 1, 2, 3, 4, 5, 6, 7], 10, false);
			
			addGraphic(beat, 3, 170, 90);
			
			
			super.begin();
		}
		
		override public function update():void 
		{			
			if (ret && Input.pressed(Key.SPACE))
			{
				FP.world = new MenuWorld;
			}
			
			super.update();
		}
		
		public function explosion():void
		{
			beat.visible = false;
			addGraphic(heart, 2, 0, 0);
			heart.play("expl");
			explode.play();
		}
		
		public function displayStats():void
		{
			var text:Text = new Text("Congratulations, you made $", 100, 200);
			text.size = 24;
			text.color = 0xffffff;
			
			var amount:Text = new Text(String(BankEntity.totalMoney), 480, 200);
			amount.size = 24;
			amount.color = 0xffffff;
			
			addGraphic(text, 1, 0, 0);
			addGraphic(amount, 1, 0, 0);
			
			var sp:Text = new Text("Press <<SPACE>> to Return to Menu", 100, 430);
			sp.size = 24;
			sp.color = 0xffffff;
			sp.align = "center";
			
			addGraphic(sp, 1, 0, 0);
			ret = true;
			
		}
	}

}