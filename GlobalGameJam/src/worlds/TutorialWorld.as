package worlds 
{
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class TutorialWorld extends World 
	{
		
		public function TutorialWorld() 
		{
			super();
		}
		
		override public function begin():void 
		{
			addGraphic(new Image(A.IMAGE_TUTORIAL));
			
			super.begin();
		}
		
		override public function update():void 
		{
			if (Input.pressed(Key.SPACE))
			{
				FP.world = new MenuWorld;
			}
			
			super.update();
		}
		
	}

}