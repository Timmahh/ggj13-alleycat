package 
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import worlds.*;
	
	public class Main extends Engine
	{
		
		public function Main():void 
		{
			super(640, 480);
			//FP.console.enable();
		}
		
		override public function init():void
		{
			FP.screen.color = 0x0000000;
			FP.world = new MenuWorld;
		}
	}
	
}