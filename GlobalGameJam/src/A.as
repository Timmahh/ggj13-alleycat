package  
{
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class A 
	{
		/** IMAGES **/
		[Embed(source = "../assets/images/player.png")] public static const IMAGE_PLAYER:Class;
		
		[Embed(source = "../assets/images/tileset.png")] public static const IMAGE_TILESET:Class;
		
		[Embed(source = "../assets/images/tileset_light.png")] public static const IMAGE_LIGHTING:Class;
		
		[Embed(source = "../assets/images/car.png")] public static const IMAGE_CAR:Class;
		
		[Embed(source = "../assets/images/pedestrian.png")] public static const IMAGE_PEDESTRIAN:Class;
		
		[Embed(source = "../assets/images/status.png")] public static const IMAGE_STATUS:Class;
		
		[Embed(source = "../assets/images/hotdog.png")] public static const IMAGE_HOTDOG:Class;
		
		[Embed(source = "../assets/images/bank.png")] public static const IMAGE_BANK:Class;
		
		[Embed(source = "../assets/images/lamp.png")] public static const IMAGE_LAMP:Class;
		
		[Embed(source = "../assets/images/shop.png")] public static const IMAGE_SHOP:Class;
		
		[Embed(source = "../assets/images/coin.png")] public static const IMAGE_COIN:Class;
		
		[Embed(source = "../assets/images/bankicon.png")] public static const IMAGE_BICON:Class;
		
		[Embed(source = "../assets/images/back.png")] public static const IMAGE_BACK:Class;
		
		[Embed(source = "../assets/images/title.png")] public static const IMAGE_TITLE:Class;
		
		[Embed(source = "../assets/images/menupointer.png")] public static const IMAGE_POINTER:Class;
		
		[Embed(source = "../assets/images/heart.png")] public static const IMAGE_HEART:Class;
		
		[Embed(source = "../assets/images/explosion.png")] public static const IMAGE_EXPLOSION:Class;
		
		[Embed(source = "../assets/images/meter.png")] public static const IMAGE_METER:Class;
		
		[Embed(source = "../assets/images/tutorial.png")] public static const IMAGE_TUTORIAL:Class;
		
		/** LEVEL DATA **/
		[Embed(source = "../assets/level/testlevel.oel", mimeType = "application/octet-stream")] public static const LEVEL_TEST:Class;
		
		[Embed(source = "../assets/level/level1.oel", mimeType = "application/octet-stream")] public static const LEVEL:Class;
		
		/** SOUNDS **/
		[Embed(source = "../assets/sounds/heartbeat.mp3")] public static const SFX_DEATH:Class;
		
		[Embed(source = "../assets/sounds/music.mp3")] public static const music:Class;
		
		[Embed(source = "../assets/sounds/coin.mp3")] public static const SFX_COIN:Class;
		
		[Embed(source = "../assets/sounds/DeathFlash.mp3")] public static const SFX_EXPLOSION:Class;
		
		[Embed(source = "../assets/sounds/bank.mp3")] public static const SFX_BANK:Class;
		
		[Embed(source = "../assets/sounds/food.mp3")] public static const SFX_FOOD:Class;
		
		[Embed(source = "../assets/sounds/hit.mp3")] public static const SFX_HIT:Class;
	}

}