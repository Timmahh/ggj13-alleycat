package entities 
{
	import entities.statemachine.AlertState;
	import entities.statemachine.RunState;
	import entities.statemachine.State;
	import entities.statemachine.StateMachine;
	import entities.statemachine.WalkState;
	import flash.display.Sprite;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class PedestrianEntity extends AIEntity 
	{
		public var image:Spritemap;
		
		public var alertTime:Number = 0.0;
		
		public var timeout:Number = 0.0;
		public var timerStarted:Boolean = false;
		public var pickTime:Number = 0.0;
		
		private var direction:int;
		private var hit:Sfx = new Sfx(A.SFX_HIT);
		
		public var moneyTaken:Boolean = false;
		
		private var coin:Sfx = new Sfx(A.SFX_COIN);
		
		public function PedestrianEntity(x:Number = 0, y:Number = 0, dir:int = 1) 
		{
			image = new Spritemap(A.IMAGE_PEDESTRIAN, 28, 54);
			image.add("right", [7, 6, 5, 4, 3, 2, 1, 0], 10, true);
			image.add("left", [9, 10, 11, 12, 13, 14, 15, 16], 10, true);
			image.add("idle", [8]);
			graphic = image;
			
			type = "pedestrian";
			setHitbox(28, 32, 0, -8);
			
			direction = dir;
			if (direction > 0)
			{
				image.play("right");
			}
			else
			{
				image.play("left");
			}
			
			stateMachine = new StateMachine(this, new WalkState(this));
			
			super(x, y);
			
			speed = 80;
		}
		
		override public function update():void 
		{
			stateMachine.update();
			
			checkPosition();
			checkMovement();
			checkState();
			
			timing();
			
			super.update();
		}
		
		public function timing():void
		{
			if (!moneyTaken)
			{
				if (timerStarted)
				{
					timeout += FP.elapsed;
				}
				
				if (pickTime > 2.0)
				{
					moneyTaken = true;
					coin.play();
					
					var player:Array = new Array;
					
					FP.world.getClass(PlayerEntity, player);
					
					for each(var pl:PlayerEntity in player)
					{
						pl.moneyStolen();
					}
				} 
				else if (timeout > 6.0 || stateMachine.currentState is AlertState)
				{
					timerStarted = false;
					timeout = 0.0;
					pickTime = 0.0;
				}
			}
		}
		
		public function checkPosition():void
		{
			if (x < -100 || x > 1380)
			{
				FP.world.remove(this);
			}
		}
		
		public function checkMovement():void
		{
			x += direction * velocity.x * speed * FP.elapsed;
			
			if (velocity.x > 0)
			{
				if (direction > 0)
				{
					image.play("right");
				}
				else
				{
					image.play("left");
				}
			}
			else if (velocity.x == 0)
			{
				image.play("idle");
			}
		}
		
		public function checkState():void
		{
			if (stateMachine.currentState is WalkState)
			{
				if (collide("player", x, y))
				{
					hit.play();
					stateMachine.currentState = new AlertState(this);
					alertTime = 0.0;
					
					// Increase the heart rate of the player
					var players:Array = new Array;
					
					FP.world.getClass(PlayerEntity, players);
					
					for each(var pl:PlayerEntity in players)
					{
						pl.heartbeat.currentHeartbeat += 10.0;
					}
				}
			}
			
			if (stateMachine.currentState is AlertState)
			{
				alertTime += FP.elapsed;
				if (alertTime > 5.0)
				{
					stateMachine.currentState = new WalkState(this);
				}
			}
			
			if (moneyTaken)
			{
				if (stateMachine.currentState is RunState)
				{}
				else
				{
					stateMachine.currentState = new RunState(this);
				}
			}
		}
		
		public function pickPocket():void
		{
			if (stateMachine.currentState is AlertState)
			{}
			else
			{
				if (!timerStarted)
				{
					timerStarted = true;
				}
				
				timeout = 0.0;
				
				pickTime += FP.elapsed;
			}
		}
	}

}