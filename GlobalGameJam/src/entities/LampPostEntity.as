package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author ...
	 */
	public class LampPostEntity extends Entity 
	{
		
		public function LampPostEntity(x:Number=0, y:Number=0) 
		{
			graphic = new Image(A.IMAGE_LAMP);
			
			super(x, y, graphic, mask);
		}
		
	}

}