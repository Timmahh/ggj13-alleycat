package entities 
{
	import entities.playerComponents.HeartbeatComponent;
	import entities.playerComponents.KeyboardComponent;
	import entities.playerComponents.WalletComponent;
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import ui.UINotificationEntity;
	import net.flashpunk.Sfx;
	import worlds.DeathWorld;
	
	public class PlayerEntity extends Entity 
	{
		public var SPEED:int = 110;

		public var velocity:Point = new Point;
		private var playerSprite:Spritemap;
		private var direction:int = 0; // 0 up, 1 left, 2 down, 3 right
		
		private var keyboard:KeyboardComponent;
		public var heartbeat:HeartbeatComponent;
		public var wallet:WalletComponent;
		
		private var notifications:UINotificationEntity = new UINotificationEntity();
		
		private var food:Sfx;
		private var bank:Sfx;
		
		public function PlayerEntity(x:Number, y:Number) 
		{
			layer = 2;
			
			playerSprite = new Spritemap(A.IMAGE_PLAYER, 64, 64);
			playerSprite.add("left", [8, 9, 10, 11], 10, true);
			playerSprite.add("right", [4, 5, 6, 7], 10, true);
			graphic = playerSprite;
			
			playerSprite.play("left");
			
			setHitbox(40, 24, -16, -18);
			type = "player";
			
			keyboard = new KeyboardComponent(this);
			heartbeat = new HeartbeatComponent(this);
			wallet = new WalletComponent(this);
			
			food = new Sfx(A.SFX_FOOD);
			bank = new Sfx(A.SFX_BANK);
			
			FP.world.add(notifications);
			
			super(x, y, graphic);
		}
		
		override public function update():void 
		{
			velocity.x = 0;
			velocity.y = 0;
			
			x = FP.clamp(x, 0, 1280 - 64);
			y = FP.clamp(y, 0, 960 - 64);
			
			SPEED = 110 + (2 * heartbeat.currentHeartbeat);
			
			keyboard.update();
			heartbeat.update();
			wallet.update();
			
			handleCollision();
			handleFacing();
			handlePickpocket();
			
			super.update();
		}
		
		public function handleCollision():void
		{
			x += velocity.x * SPEED * FP.elapsed;
			
			if (collide("solid", x, y))
			{
				x -= velocity.x * SPEED * FP.elapsed;
			}
			
			y += velocity.y * SPEED * FP.elapsed;
			
			if (collide("solid", x, y))
			{
				y -= velocity.y * SPEED * FP.elapsed;
			}
			
			if (collide("light", x, y))
			{
				heartbeat.currentHeartbeat += 0.03;
			}
			else
			{
				heartbeat.currentHeartbeat -= 0.01;
			}
			
			notifications.shop = false;
			notifications.bank = false;
			if (collide("shop", x, y))
			{
				notifications.shop = true;
			}
			
			if (collide("bank", x, y))
			{
				notifications.bank = true;
			}
			
			if (collide("car", x, y))
			{
				dead();
			}
		}
		
		public function handleFacing():void
		{
			if (velocity.x > 0)
			{
				direction = 1;
				playerSprite.play("right");
			}
			else if (velocity.x < 0)
			{
				direction = 3;
				playerSprite.play("left");
			}
		}
		
		public function handlePickpocket():void
		{
			var close:Array = new Array;
			
			collideInto("pedestrian", x + (velocity.x * 32), y, close)
			
			for each(var ped:PedestrianEntity in close)
			{
				ped.pickPocket();
			}
		}
		
		public function moneyStolen():void
		{
			wallet.addMoney();
		}
		
		public function action():void
		{			
			var shops:Array = new Array;
			
			collideInto("shop", x, y, shops);
			
			for each(var sh:ShopEntity in shops)
			{
				if (sh.buyHotdog() && wallet.amount >= 10.0)
				{
					heartbeat.currentHeartbeat = heartbeat.minHeartbeat;
					wallet.amount -= 10.0;
					
					food.play();
				}
			}
			
			if (collide("bank", x, y))
			{
				BankEntity.deposit(wallet.depositMoney());
				
				bank.play();
			}
		}
		
		public function dead():void
		{
			FP.world = new DeathWorld;
		}
	}

}