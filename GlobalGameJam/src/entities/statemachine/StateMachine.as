package entities.statemachine 
{
	import entities.AIEntity;
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class StateMachine 
	{
		private var entity:AIEntity;
		
		public var currentState:State;
		
		public function StateMachine(e:AIEntity, init:State) 
		{
			entity = e;
			currentState = init;
		}
		
		public function update():void
		{
			currentState.update();
		}	

	}

}