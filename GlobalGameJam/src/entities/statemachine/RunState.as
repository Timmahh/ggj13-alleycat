package entities.statemachine 
{
	import entities.AIEntity;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class RunState extends State 
	{
		// pause for a second then run away
		private var pause:Number = 0.0;
		
		public function RunState(e:AIEntity) 
		{		
			super(e);
		}
		
		override public function update():void 
		{
			pause += FP.elapsed;
			
			if (pause > 1.5)
			{
				entity.velocity.x = 3;
			}
			
			super.update();
		}
	}

}