package entities.statemachine 
{
	import entities.AIEntity;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class WalkState extends State 
	{
		
		public function WalkState(e:AIEntity) 
		{
			super(e);
		}
		
		override public function update():void 
		{
			entity.velocity.x = 1;
			
			super.update();
		}
		
	}

}