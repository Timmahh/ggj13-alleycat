package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author ...
	 */
	public class BankEntity extends Entity 
	{
		public static var totalMoney:Number = 0.0;
		
		public function BankEntity(x:Number=0, y:Number=0) 
		{
			layer = 5;
			
			totalMoney = 0.0;
			
			graphic = new Image(A.IMAGE_BANK);
			
			type = "bank";
			setHitbox(256, 96, 0, 0);
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{		
			super.update();
		}
		
		public static function deposit(amount:Number):void
		{
			totalMoney += amount;
		}
	}

}