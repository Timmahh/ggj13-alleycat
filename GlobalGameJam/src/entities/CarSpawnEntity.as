package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class CarSpawnEntity extends Entity 
	{
		private var direction:int;
		private var timer:Number;
		private var alarm:int;
		
		public function CarSpawnEntity(x:Number = 0, y:Number = 0, dir:int = 1) 
		{
			direction = dir;
			
			alarm = 2 + FP.rand(6);
			timer = 0.0;
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			timer += FP.elapsed;
			
			if (timer > alarm)
			{
				spawnCar();
				alarm = 2 + FP.rand(2);
				timer = 0.0;
			}
			
			super.update();
		}
		
		public function spawnCar():void
		{
			FP.world.add(new CarEntity(x, y, direction));
		}
	}

}