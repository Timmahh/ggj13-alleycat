package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	public class PedestrianSpawnEntity extends Entity 
	{
		private var direction:int;
		private var timer:Number;
		private var alarm:int;
		
		public function PedestrianSpawnEntity(x:Number = 0, y:Number = 0, dir:int = 1) 
		{
			timer += FP.elapsed;
			
			alarm = 3 + FP.rand(4);
			timer = 0.0;
			
			direction = dir;
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			timer += FP.elapsed;
			
			if (timer > alarm)
			{
				spawnPedestrian();
				alarm = 3 + FP.rand(4);
				timer = 0.0;
			}
			
			super.update();
		}
		
		public function spawnPedestrian():void
		{
			var ped:PedestrianEntity = new PedestrianEntity(x, y - FP.rand(32), direction);
			var status:StatusEntity = new StatusEntity(ped);
			
			FP.world.add(ped);
			FP.world.add(status);
		}
	}

}