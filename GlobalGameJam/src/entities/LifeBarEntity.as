package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class LifeBarEntity extends Entity 
	{
		private var im:Image = Image.createRect(20, 3, 0xff0000);
		private var pl:PlayerEntity;
		
		public function LifeBarEntity(x:Number, y:Number, player:PlayerEntity) 
		{
			layer = -2;
			graphic = im;
			pl = player;
			
			x = FP.world.camera.x + 600;
			y = FP.world.camera.y + 390;
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			x = FP.world.camera.x + 600;
			y = FP.world.camera.y + 390;
			
			im.scaleY = -pl.heartbeat.currentHeartbeat;
			
			super.update();
		}
	}

}