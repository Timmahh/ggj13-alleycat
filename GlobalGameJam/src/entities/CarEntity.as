package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class CarEntity extends Entity 
	{
		private const speed:Number = FP.rand(50) + 200;
		private var direction:int;
		
		public function CarEntity(x:Number=0, y:Number=0, dir:int = 1) 
		{
			layer = 2;
			
			var im:Image = new Image(A.IMAGE_CAR);
			
			direction = dir;
			
			if (direction > 0)
			{
				im.flipped = true;
			}
			
			graphic = im;			
			
			type = "car";
			setHitbox(64, 32, -8, -16);
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			x += direction * speed * FP.elapsed;
			
			if (x < -100 || x > 2560)
			{
				FP.world.remove(this);
			}
			
			super.update();
		}
		
	}

}