package entities 
{
	import entities.statemachine.StateMachine;
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class AIEntity extends Entity 
	{
		protected var speed:Number = FP.rand(50) + 100;
		public var velocity:Point;
		
		public var stateMachine:StateMachine;
		
		public function AIEntity(x:Number=0, y:Number=0) 
		{			
			velocity = new Point();
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			velocity.x = 0;
			velocity.y = 0;
			
			super.update();
		}
	}

}