package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ShopEntity extends Entity 
	{
		private var soldOut:Boolean = false;
		private var restockTime:Number = 0.0;	
		
		public function ShopEntity(x:Number=0, y:Number=0) 
		{
			layer = 3;
			
			graphic = new Image(A.IMAGE_SHOP);
			type = "shop";
			setHitbox(96, 96, 24, 0);
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			if (soldOut)
			{
				restockTime += FP.elapsed;
			}
			
			if (restockTime > 10.0)
			{
				soldOut = false;
				restockTime = 0.0;
			}
			
			super.update();
		}
		
		public function buyHotdog():Boolean
		{
			if (soldOut)
			{
				return false;
			}
			else
			{
				soldOut = true;
				return true;
			}
		}
	}

}