package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.masks.Grid;
	import net.flashpunk.FP;
	
	public class LevelEntity extends Entity
	{
		private var tiles:Tilemap;
		private var grid:Grid;
		
		public function LevelEntity(mapXML:XML) 
		{		
			layer = 5;
			
			tiles = new Tilemap(A.IMAGE_TILESET, 1280, 960, 32, 32);
			
			tiles.loadFromString(String(mapXML.Tiles));
			type = "solid";
			
			grid = new Grid(uint(mapXML.@width), uint(mapXML.@height), 8, 8, 0, 0);
			grid.loadFromString(String(mapXML.Collision), "", "\n");
			
			super(0, 0, tiles, grid);
		}
		
	}

}