package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.masks.Grid;
	
	/**
	 * ...
	 * @author Timothy Veletta
	 */
	public class LightmapEntity extends Entity 
	{
		private var tiles:Tilemap;		
		private var grid:Grid;
		
		public function LightmapEntity(mapXML:XML) 
		{
			layer = 4;
			
			tiles = new Tilemap(A.IMAGE_LIGHTING, 2560, 1920, 16, 16);
			
			tiles.loadFromString(String(mapXML.LightTiles));			
			type = "light";
			
			grid = new Grid(uint(mapXML.@width), uint(mapXML.@height), 8, 8, 0, 0);
			grid.loadFromString(String(mapXML.LightMap), "", "\n");
			
			super(0, 0, tiles, grid);
		}
		
	}

}