package entities 
{
	import entities.statemachine.AlertState;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Mask;
	
	public class StatusEntity extends Entity 
	{
		/**
		 * 0 none
		 * 1 alert
		 * 2 clueless
		 * 3
		 * 4 half
		 * 5
		 * 6 taken
		 */
		public var status:int = 0;
		private var statusSprite:Spritemap;
		
		private var pedestrian:PedestrianEntity;
		
		public function StatusEntity(ped:PedestrianEntity)
		{
			layer = 0;
			
			statusSprite = new Spritemap(A.IMAGE_STATUS, 16, 16);
			statusSprite.add("idle", [0]);
			statusSprite.add("alert", [1]);
			statusSprite.add("clueless", [2]);
			statusSprite.add("whole", [3]);
			statusSprite.add("3q", [4]);
			statusSprite.add("half", [5]);
			statusSprite.add("1q", [6]);
			statusSprite.add("none", [7]);
			graphic = statusSprite;
			
			pedestrian = ped;
			x = ped.x + 10;
			y = ped.y - 16;
			
			super(x, y, graphic, mask);
		}
		
		override public function update():void 
		{
			this.x = pedestrian.x + 6;
			this.y = pedestrian.y - 16;
			
			statusSprite.play("idle");
			
			if (pedestrian.stateMachine.currentState is AlertState)
			{
				statusSprite.play("alert");
			}
			
			if (pedestrian.pickTime > 0.0)
			{
				statusSprite.play("whole");
			}
			if (pedestrian.pickTime > 0.5)
			{
				statusSprite.play("3q");
			}
			if (pedestrian.pickTime > 1)
			{
				statusSprite.play("half");
			}
			if (pedestrian.pickTime > 1.5)
			{
				statusSprite.play("1q");
			}
			if (pedestrian.pickTime >= 2)
			{
				statusSprite.play("none");
			}
			
			if (pedestrian.moneyTaken)
			{
				statusSprite.play("clueless");
			}
			
			super.update();
		}
		
	}

}