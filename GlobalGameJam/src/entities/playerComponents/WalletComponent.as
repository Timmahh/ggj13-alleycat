package entities.playerComponents 
{
	import entities.PlayerEntity;
	
	public class WalletComponent 
	{
		private var entity:PlayerEntity;
		
		public var amount:Number = 0.0;
		public var multiplier:Number = 1;
		
		public function WalletComponent(e:PlayerEntity) 
		{
			entity = e;
		}
		
		public function update():void
		{
			if (amount >= 20)
			{
				entity.heartbeat.currentHeartbeat += (((amount / 2) - 8) / 200);
			}
			else
			{
				entity.heartbeat.currentHeartbeat -= 0.02;
			}
		}
		
		public function addMoney():void
		{
			amount += (2 * multiplier) + 8;
			
			multiplier++;
		}
		
		public function depositMoney():Number
		{
			var am:Number = amount;
			multiplier = 1;
			amount = 0.0;
			
			return am;
		}
	}

}