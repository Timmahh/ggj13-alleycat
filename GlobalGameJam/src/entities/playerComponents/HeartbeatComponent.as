package entities.playerComponents 
{
	import entities.PlayerEntity;
	import net.flashpunk.FP;

	public class HeartbeatComponent 
	{
		private var entity:PlayerEntity;
		
		public var currentHeartbeat:Number = 0.0;
		public var maxHeartbeat:Number = 0.0;
		public var minHeartbeat:Number = 0.0;
		
		public var heartAttack:Boolean = false;
		
		public function HeartbeatComponent(e:PlayerEntity) 
		{
			entity = e;
		}
		
		public function update():void
		{
			if (currentHeartbeat >= maxHeartbeat)
			{
				maxHeartbeat = currentHeartbeat;
			}
			
			if (currentHeartbeat > 100.0)
			{
				heartAttack = true;
			}
			
			minHeartbeat = maxHeartbeat - 20.0;
			
			if (minHeartbeat < 0.0)
			{
				minHeartbeat = 0.0;
			}
			
			if (currentHeartbeat <= minHeartbeat)
			{
				currentHeartbeat = minHeartbeat;
			}
			
			if (currentHeartbeat > 100)
			{
				entity.dead();
			}
		}
	}

}