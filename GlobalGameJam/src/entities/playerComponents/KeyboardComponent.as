package entities.playerComponents 
{
	import entities.PlayerEntity;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	
	public class KeyboardComponent 
	{
		private var player:PlayerEntity;
		
		public function KeyboardComponent(entity:PlayerEntity) 
		{
			Input.define("left", Key.LEFT);
			Input.define("right", Key.RIGHT);
			Input.define("up", Key.UP);
			Input.define("down", Key.DOWN);
			
			Input.define("action", Key.SPACE);
			
			player = entity;
		}
		
		public function update():void
		{
			if (Input.check("left"))
			{
				player.velocity.x -= 1;
			}
			
			if (Input.check("right"))
			{
				player.velocity.x += 1;
			}
			
			if (Input.check("up"))
			{
				player.velocity.y -= 1;
			}
			
			if (Input.check("down"))
			{
				player.velocity.y += 1;
			}
			
			if (Input.pressed("action"))
			{
				player.action();
			}
		}
	}

}